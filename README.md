# DroppleR
Python utility that allows to simply setup a development VM out of a set of previously made Tings (ready to use Vagrant boxes with an advanced provision stage).
Droppler will:
 - Create a working VM in minutes
 - Be incredibly user friendly
 - Be incredibly easy to use

### Usage
Droppler only needs to be started, will then start to ask questions. Droppler can be ran normally as a python script using `python droppler.py`, or can be installed on the system by running `python droppler.py install`. This will make Droppler available on the whole system by simply running `droppler`.
If installed on the system, Droppler can be updated by running `droppler update` or removed by running `droppler remove`.
Being a Python script, Droppler can be stopped at any time by using `CTRL+C`.

### Requirements
 - Git
 - Vagrant
     - Hostmanager (vagrant extension): ```vagrant plugin install vagrant-hostmanager```
 - VirtualBox / any other [suitable provider](https://www.vagrantup.com/docs/providers/)

### Overview
*to be written*

# Credits
 - Filippo <kn0t> Teodori - Eligent Group - filippo@eligent.group
     - Core Development

# License 
Copyleft - All Wrongs Reversed

