#! /usr/bin/env python
# -*- coding: utf-8 -*-
import os
from sys import argv
from random import randint
from shutil import move
import json

###########################################################
# CONSTANTS
version = "4.1.3"

RESET = '\033[0m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
CYAN = "\033[0;36m"

droppler_repo = "git@bitbucket.org:kn0t/droppler.git"
tings = {
			1:{
				"name": "LEMP Starter Ting", 
				"URL": "git@bitbucket.org:kn0t/lemp-starter-ting.git"
			},2:{
				"name": "WordPress Starter Ting",
				"URL": "git@bitbucket.org:kn0t/wp-starter-ting.git"
			}
			,3:{
				"name": "Laravel Starter Ting",
				"URL": "git@bitbucket.org:Richard_Phelps123/laravel-starter-ting.git"
			}
		}


###########################################################
# FUNCTIONS
def task(string, prefix = "="):
	if len(string) < 75:
		strlen = len(string)
		prfxlen = len(prefix)
		strdiff = (75 - (strlen + prfxlen)) - 4
		i = 0;
		complete = " "
		while i != strdiff:
			complete = complete+"="
			i = i + 1
	print("\n["+prefix+"] "+string+complete)

def success(string = "DONE", noprefix = False):
	if noprefix == True:
		print(GREEN+string+RESET)
	else:
		print(GREEN+"[+] "+string+RESET)

def warning(string = "WARNING", noprefix = False):
	if noprefix == True:
		print(YELLOW+string+RESET)
	else:
		print(YELLOW+"[i] "+string+RESET)

def error(string = "ERROR", noprefix = False):
	if noprefix == True:
		print(RED+string+RESET)
	else:
		print(RED+"[!] "+string+RESET)

def do_silent(command):
	os.system(command+" &>/dev/null");

def replace_in_file(file_path, search, replace):
	try:
		fh = open(file_path, "r")
		file_contents = fh.read();

		if search not in file_contents:
			error(file_path+" has no match: "+search)
		else:
			new_file_contents = file_contents.replace(search, replace)
			fh.close();

			try:
				fh = open(file_path, "w+")
				fh.write(new_file_contents);
				fh.close();
				success("OK")
			except:
				error("Can't write on file: "+file_path)
	except:
		error("Error writing file: "+file_path)

def droppler_install(dry=False):
	randomify = str(randint(1,181818));
	droppler_tmp_dir = "/tmp/droppler"+randomify
	droppler_tmp_file = droppler_tmp_dir+"/droppler.py"
	droppler_tmp_json = droppler_tmp_dir+"/droppler.json"

	if dry is False:
		task("Cloning Droppler into "+droppler_tmp_dir+"...")
		os.system("git clone "+droppler_repo+" "+droppler_tmp_dir)

		task("Checking files...")
		if os.path.isfile(droppler_tmp_file):
			success("File found: "+droppler_tmp_file)
		else:
			error("Can't find file: "+droppler_tmp_file+"... interrupting");
			exit();

		task("Moving "+droppler_tmp_file+" to /usr/local/bin/droppler")
		try:
			move(droppler_tmp_file, "/usr/local/bin/droppler");
			success("Success");
			
			task("Setting executable permissions...")
			try:
				os.system("chmod a+x /usr/local/bin/droppler")
				success("Success")
				print
				warning("You can now use Droppler by simply typing 'droppler' in your CLI.")
				warning("Use 'droppler uninstall' to uninstall and 'droppler update' to update!")
			except:
				error("Failed to chmod file. Maybe try with root:")
				print("sudo chmod a+x /usr/local/bin/droppler")
				exit();

			if os.path.isdir("/usr/local/lib/droppler"):
				task("Copying droppler.json to /usr/local/lib/droppler/droppler.json")
				try:
					move(droppler_tmp_json, "/usr/local/lib/droppler/droppler.json");
					success("Success")
				except:
					error("Failed!")
			else:
				task("Creating /usr/local/lib/droppler...")
				try:
					os.system("mkdir /usr/local/lib/droppler")
					task("Copying droppler.json to /usr/local/lib/droppler/droppler.json")
					success("Success")
				except:
					error("Failed!")

				try:
					move(droppler_tmp_json, "/usr/local/lib/droppler/droppler.json");
					success("Success")
				except:
					error("Failed!")

			task("Removing "+droppler_tmp_dir)
			try:
				os.system("rm -rf "+droppler_tmp_dir)
				success("Success")
			except:
				error("Couldn't remove: "+droppler_tmp_dir)

			print
			print
		except:
			error("Operation failed - please try with sudo? ")
	else:
		do_silent("git clone "+droppler_repo+" "+droppler_tmp_dir)
		if not os.path.isfile(droppler_tmp_file):
			error("Can't find file: "+droppler_tmp_file+"... interrupting");
			exit();
		try:
			move(droppler_tmp_file, "/usr/local/bin/droppler");
			try:
				os.system("chmod a+x /usr/local/bin/droppler")
			except:
				error("Failed to chmod file. Maybe try with root:")
				print("sudo chmod a+x /usr/local/bin/droppler")
				exit();

			try:
				os.system("rm -rf "+droppler_tmp_dir)
			except:
				error("Couldn't remove: "+droppler_tmp_dir)
		except:
			error("Operation failed - please try with sudo? ")

def droppler_update():
	if os.path.isfile("/usr/local/bin/droppler"):
		success("Updating droppler...")
		droppler_install();
	else:
		error("Droppler not installed on this system :O")
		warning("Install droppler by running it with 'install'")

def droppler_uninstall():
	if os.path.isfile("/usr/local/bin/droppler"):
		success("Removing Droppler...")
		try:
			os.remove("/usr/local/bin/droppler")
			success("Droppler uninstalled. Sorry to see you go.")
			print
			print
		except:
			error("Couldn't remove: /usr/local/bin/droppler")
			warning("Maybe try with sudo?")
	else:
		error("Droppler not installed on this system :O")
		warning("Install droppler by running 'droppler install'")


def droppler_deploy(block):
	if os.path.isfile("droppler.json"):
		success("Reading droppler.json...")
		d = open("droppler.json","r")
		droppler_json = d.read();
		d.close();
		try:
			droppler = json.loads(droppler_json)
			success("File read fine.")
		except:
			error("Invalid JSON structure in droppler.json!")
			exit();


		if block == False:
			username = droppler["connection"]["user"]
			hostname = droppler["connection"]["host"]
			warning("Hostname: "+hostname)
			warning("Username: "+username)
			for deployment in droppler["deploy"]:
				scp = "scp -r [localfolder] "+username+"@"+hostname+":[remotefolder]";
				#scp = 'rsync -Lavz -e "ssh -i <full-path-to-pem> -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --exclude "*.something --progress <path inside local host> <user>@<host>:<path inside remote host>'.
				local_path = deployment["local"]
				remote_path = deployment["remote"]
				task("DEPLOY: "+local_path+" => "+remote_path)
				scp = scp.replace("[localfolder]", local_path);
				deployment_command = scp.replace("[remotefolder]", remote_path)
				os.system(deployment_command)
		else:
			username = droppler["connection"]["user"]
			hostname = droppler["connection"]["host"]
			warning("Hostname: "+hostname)
			warning("Username: "+username)
			warning("Block: "+block)
			found = False
			for deployment in droppler["deploy"]:
				try:
					this_block = deployment["name"]
					if block == this_block:
						found = True
						success("Found block: "+block)
						scp = "scp -r [localfolder] "+username+"@"+hostname+":[remotefolder]";
						#scp = 'rsync -Lavz -e "ssh -i <full-path-to-pem> -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --exclude "*.something --progress <path inside local host> <user>@<host>:<path inside remote host>'.
						local_path = deployment["local"]
						remote_path = deployment["remote"]
						task("DEPLOY: "+local_path+" => "+remote_path)
						# scp = scp.replace("[localfolder]", local_path);
						# deployment_command = scp.replace("[remotefolder]", remote_path)
						# os.system(deployment_command)
					else:
						warning("Skipping block: "+this_block)
				except:
					warning("Skipping unnamed block.")
			if found == False:
				error("Can't find block: "+block)

	else:
		error("File droppler.json not found")
		exit();

	success("End of deployment.")

def droppler_vm():
	###########################################################
	# GET REPOSITORY URL
	print("Please provide a valid URL pointing to a git repository.")
	print("e.g. git@bitbucket.org:username/project.git")
	print("- use the 'clone' button")
	print("- you can leave this blank if you don't want to auto-push")
	print
	print
	warning("Quit with CTRL+C and use the parameter 'help' for... you know.")
	warning("Run droppler followed by 'install' to install on your system")
	print
	repo_url = raw_input(">>> ")
	if repo_url != "":
		success("Cheers! Git URL: "+repo_url)
	print
	print

	###########################################################
	# CHOOSE PACKAGE
	print("===========================================================================")
	print("Please select your desired TING:")
	i = 1;
	for ting in tings:
		ting = tings[int(i)]
		ting_name = ting["name"]
		ting_url = ting["URL"]
		print(str(i)+"] "+ting_name)
		i = i + 1;

	print
	ting_id = raw_input(">>> ")
	try:
		ting = tings[int(ting_id)]
	except:
		error("Invalid fucking choice dude. This was the easiest bit...")
		exit()
	ting = tings[int(ting_id)]
	ting_name = ting["name"]
	ting_url = ting["URL"]
	success("Alright, using "+ting_name+" ("+ting_url+")")
	print
	print


	###########################################################
	# SELECT DESTINATION FOLDER
	print("===========================================================================")
	print("Please provide a folder name for your project (will be created here),")
	print("or leave blank for current folder")
	print
	clone_to = raw_input(">>> ")


	###########################################################
	# CLONING PACKAGE
	if clone_to == "":
		warning("Cloning here...")
		try:
			do_silent("git clone "+ting_url)
			vm_location = working_path
		except:
			error("git has thrown a fatal error! Please try manually:")
			error("git clone "+ting_url)
	else:
		clone_to_path = os.path.abspath(clone_to)
		warning("Cloning into "+clone_to_path+"...")
		try:
			do_silent("git clone "+ting_url+" "+clone_to)
			vm_location = clone_to_path
		except:
			error("git has thrown a fatal error! Please try manually:")
			error("git clone "+ting_url+" "+clone_to)
			exit()
	warning("Switching to branch 'droppler'...")
	os.chdir(vm_location)
	os.system("git checkout droppler")
	os.chdir(working_path)
	print
	print


	###########################################################
	# IP & HOSTNAME SETUP
	print("===========================================================================")
	print("Please choose a hostname for your VM (e.g. goddo.vm):")
	vm_hostname = raw_input(">>> ")
	success("Hostname: "+vm_hostname)
	warning("Trying to read /etc/hosts...")
	try:
		hf = open("/etc/hosts", "r")
		hosts_file = hf.read()
		hf.close()
		success("File read fine.")
	except:
		error("Unable to read file. Can't check if IP is unique");
		warning("Generating one anyway...")
		hosts_file = False;

	if hosts_file is not False:
		unique = False
		while unique == False:
			block_1 = randint(0,255)
			block_2 = randint(0,255)
			test_ip = "13.37."+str(block_1)+"."+str(block_2)
			warning("Checking IP: "+test_ip);
			if test_ip not in hosts_file:
				vm_ip = test_ip
				unique = True
				success("IP is valid: "+vm_ip)
			else:
				error("IP not unique, regenerating...")
	else:
		block_1 = randint(0,255)
		block_2 = randint(0,255)
		vm_ip = "13.37."+str(block_1)+"."+str(block_2)

	success("Using IP: "+vm_ip)


	###########################################################
	# APPLYING CHANGES TO VAGRANT AND NGINX
	vm_vagrantfile = vm_location+"/Vagrantfile"
	vm_nginxconf = vm_location+"/provision_files/nginx_vhost"
	if os.path.isfile(vm_vagrantfile) and os.path.isfile(vm_nginxconf):
		warning("Configuring Vagrantfile...")
		try:
			replace_in_file(vm_vagrantfile, "[droppler-hostname]", vm_hostname)
			replace_in_file(vm_vagrantfile, "[droppler-ip]", vm_ip)
		except:
			error("Can't write on Vagrant file ("+vm_vagrantfile+")")
		warning("Configuring Nginx conf...")
		try:
			replace_in_file(vm_nginxconf, "[droppler-hostname]", vm_hostname)
		except:
			error("Can't write on Nginx's provision file: "+vm_nginxconf)
	else: 
		print("Vagrantfile not found")


	###########################################################
	# REMOVE CURRENT GIT INSTANCE
	warning("Removing git instance from starter repo...")
	os.chdir(vm_location)
	os.system("rm -rf .git")
	os.chdir(working_path)


	###########################################################
	# SETTING UP GIT REPOSITORY
	if repo_url != "":
		warning("Initialising new git repository...")
		os.chdir(vm_location)
		do_silent("git init")
		warning("Trying to set remote to "+repo_url+"...")
		try:
			do_silent("git remote add origin "+repo_url)
			success("Success")
		except:
			error("Operation failed")
		warning("Trying commit & push...")
		try:
			warning("Updating git repository: "+repo_url)
			do_silent("git add -A")
			do_silent("git commit -m 'droppler: base VM'")
			do_silent("git push -u -f origin master")
			success("Pushed all changes to: "+repo_url)
			os.chdir(working_path)
		except:
			error("Can't push changes to: "+repo_url)


	###########################################################
	# STARTING UP VAGRANT AND RUNNING PROVISION
	warning("Trying to run and provide Vagrant box...")
	try:
		os.chdir(vm_location)
		os.system("vagrant up --provision")
	except:
		error("Couldn't run Vagrant!")
	os.chdir(working_path)

	###########################################################
	# UPGRADING BOX IF NECESSARY
	task("Trying to update box...")
	try:
		os.chdir(vm_location)
		os.system("vagrant box update")
	except:
		error("Operation failed")

	success("Bye! <3")

def droppler_make_deploy(dest):
	task("Getting sample droppler.json...")
	try:
		os.system("cp /usr/local/lib/droppler/droppler.json "+dest)
		success("Success")
	except:
		error("Copy failed: /usr/local/lib/droppler/droppler.json >> "+dest)



###########################################################
# INSTALLATION CHECK
if not os.path.isfile("/usr/local/bin/droppler"):
	success("Hey! Droppler doesn't seem to be installed on your system :)\n\nInstalling Droppler allows you to do amazing things such as:\n\t- run it by simply typing 'droppler'\n\t- always have the latest version")
	print
	install_droppler = raw_input("Type 'yeah' to install Droppler or 'meh' to carry on: ")
	if install_droppler == "yeah":
		droppler_install()
	else:
		warning("Fair enough, proceeding...")
else:
	os.system("clear")
	warning("Loading latest version...")
	droppler_install(True);


###########################################################
# INITIAL SCREEN
os.system("clear");
print("===========================================================================")
print("DROPPLER mothafuckkks!!! - Version "+version)
print("an eligent ting by kn0t - filippo@eligent.group")
print("===========================================================================")
print
print
working_path	=	os.path.abspath(".")

try:
	command = argv[1]
	if command == "install":
		droppler_install();
	elif command == "update":
		droppler_update();
	elif command == "uninstall":
		droppler_uninstall();
	elif command == "deploy":
		try:
			block = argv[2]
		except:
			block = False;
		droppler_deploy(block);
	elif command == "vm":
		droppler_vm();
	elif command == "make-deploy":
		try:
			dest = argv[2]
		except:
			dest = "."
		droppler_make_deploy(dest)
	elif command == "help":
		raise Exception;
	else:
		error(command+": not a valid command.")
except:
	print("Usage:")
	print("- python droppler.py [<argument>]\t(if using script)")
	print("- droppler [<argument>]\t\t\t(if installed)")
	print
	print
	print("ARGUMENTS:\t\tINFO:")
	print("(none) / help\t\tget help")
	print("install\t\t\tmanually install Droppler on host system (optional)")
	print("update\t\t\tmanually update Droppler version (automatic if installed)")
	print("uninstall\t\tremove Droppler from host system (if installed)")
	print("vm\t\t\tstart the tool to create a VM <3")
	print("make-deploy\t\tCreates a sample 'droppler.json' in the current folder (to use deploy)")
	print("deploy [block]\t\tDeploys a block to remote if one is specified. Otherwise deploys all blocks.")
	print
	exit();



